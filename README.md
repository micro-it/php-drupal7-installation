# README #
This repository installs a instance of [Drupal](https://www.drupal.org/)

## Requirements ##
* Apache 2.x
* PHP 5.4 or higher
* MySQL 5.0.15 or higher
* GD Graphics Library
* Apache 'mod_rewrite' module

## Installation ##
Run the following command:
~~~~
composer install
~~~~