<?php
namespace Microit\DrupalInstallation;

use Composer\Script\Event;

class Phing {

    public static function install(Event $event) {
        $phingPath = getcwd()."/vendor/phing/phing/classes";
        set_include_path(
            $phingPath .
            PATH_SEPARATOR .
            get_include_path()
        );

        require_once($phingPath.'/phing/Phing.php');
        \Phing::startup();
        
        $args = array('-f','build.xml');
        
        \Phing::fire($args);
        \Phing::shutdown();
    }
}